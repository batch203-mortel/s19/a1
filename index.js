let userName;
let password;
let role;


function getUserName(){
 	userName= prompt("Please type your username:");
	if (userName === "" || userName === null){
		alert("Username must not be empty.");
	}
	else{
		password = prompt("Please type your password:");

		if (password === "" || password === null){
			alert("Password must not be empty.")
		}

		else{
			role = prompt("Please type your role:")
			if (role === ""){
				alert("Role must not be empty.");
			}

			else if(role === null){
				alert("Role must not be empty.");
			}

			else{
				switch(role){
					case "admin":
						alert("Welcome back to the class portal, admin!");
						break;
					case "teacher":
						alert("Thank you for logging in, teacher!")
						break;
					case "student": 
						alert("Welcome to the class portal, student!");
						break;
					default:
						alert("Role out of range.");
						break;
							}
				}

		}
	}
}

console.log(getUserName())


function getAverage(num1, num2, num3, num4){
	let average = (num1 + num2 + num3 + num4)/4
	let avg = average.toFixed(2);

	if (avg < 75){
			console.log("Hello, " + userName + ", your average is " + avg + ". The equivalent letter is F." )
		}

		else if(avg < 80){
			console.log("Hello, " + userName + ", your average is " + avg + ". The equivalent letter is D." )
		}

		else if (avg < 85){
			console.log("Hello, " + userName + ", your average is " + avg + ". The equivalent letter is C." )
		}

		else if (avg < 90){
			console.log("Hello, " + userName + ", your average is " + avg + ". The equivalent letter is B." )
		}

		else if (avg < 96){
			console.log("Hello, " + userName + ", your average is " + avg + ". The equivalent letter is A." )
		}

		else if (avg < 101){
			console.log("Hello, " + userName + ", your average is " + avg + ". The equivalent letter is A+." )
		}

		else{
			console.log("Hello, " + userName + ", your average is " + avg + " and it is out of range.")
		}

}



